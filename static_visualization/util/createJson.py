import json
import urllib2
from pprint import pprint
import time

with open('../input/data.json', 'r') as json_file:
    data = json.load(json_file)
    
    text = data['text']
    value = data['value']
    
    print len(value[0])

    for i in range(len(value[0])):
    	temp_json = {}

    	temp_text = []
    	temp_value = []

    	for j in range(len(value)):
    		if (value[j][i] > 0):
    			temp_text.append(text[j])
    			temp_value.append(value[j][i])

    	temp_json['text'] = temp_text
    	temp_json['value'] = temp_value

    	output = "../input/" + str(i + 1) + '.json'

    	with open(output, 'w') as outfile:
  			json.dump(temp_json, outfile)

        if (i % 2 == 1):
           time.sleep(10)

		
