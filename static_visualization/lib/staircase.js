function reset_occu_y(mode) {
    occu_shown = new Array();
    occu_shown = occu_shown.concat(staircase[query_id].query_occu).slice(0);

    x = d3.scale.linear().domain([0, 1]).range([margin_staircase * 5, w_staircase]),
    y = d3.scale.linear().domain([d3.min(occu_shown) / 1.09, d3.max(occu_shown) * 1.209]).range([margin_staircase * 0.6 + base_staircase * 1, margin_staircase * 0.4 + 20]);
}

var color_image = Array("#1f77b4", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#dbdb8d", "#660066", "#663366", "#666666", "#669966", "#66cc66", "#66ff66", "#669966", "#996699", "#AFAFCA", "#998066", "#CC99FF", "#2E8A5C", "#CC6666", "#CCCC66", "#661A4C", "#7A1F5C", "#8F246B", "#A3297A", "#B82E8A", "#CC3399", "#D147A3", "#D65CAD", "#DB70B8", "#1F1F5C", "#24246B", "#29297A", "#2E2E8A", "#333399", "#4747A3", "#5C5CAD", "#7070B8", "#8585C2", "#663300", "#7A3D00", "#8F4700", "#A35200", "#B85C00", "#CC6600", "#D17519", "#D68533", "#DB944D", "#E0A366");

var w_staircase = $(window).width() - 70;
var h_staircase = 1000;//Math.max($(window).height(), 10) - 140;

var staircase_height = 0;

var font_size_const = 0.5;

var margin_staircase = 38;
var border_staircase = 2;
var base_staircase = 85;

var col_num = 23;
var row_num = 48;

var stair_image_height = 14;

var waterfall;
var waterfall_intensity_1;
var waterfall_intensity_2

var color_staircase_1;
var color_staircase_2;

var occurrence_intensity;
var waterfall_font_size;

var data_occu;
var data_occu_null;
var data_occu_block;
var data_occu_image;

var rules;
var line_occu;

var vertical_text;

var json_value = new Array();
var json_text = new Array();
var staircase_horizontal_tick = new Array();

function setup_waterfall() {
    waterfall = d3.select("#image_sector_stair")
        .append("svg:svg")
        .attr("class", "chart")
        .attr("width", w_staircase + 30)
        .attr("height", h_staircase + base_staircase + 20)
        .append("svg:g");
    color_staircase_1 = colorbrewer.Greens[9];
    color_staircase_1.shift();

    color_staircase_2 = colorbrewer.Greens[9];
    color_staircase_2.shift();
    color_staircase_2.shift();
    color_staircase_2.shift();

    occurrence_intensity = d3.scale.linear().domain([d3.min(staircase[query_id].query_occu),
            d3.max(staircase[query_id].query_occu)
    ]).range(["#FFCCCC", "red"]);
    waterfall_font_size = 10;

    waterfall.append("svg:line")
        .attr("x1", margin_staircase * 0.5)
        .attr("y1", margin_staircase * 1.5 + base_staircase - 2)
        .attr("x2", margin_staircase * 5 + w_staircase - margin_staircase * 5)
        .attr("y2", margin_staircase * 1.5 + base_staircase - 2)
        .attr("fill", "white")
        .style("fill-opacity", 0)
        .style("stroke-width", "0.5px")
        .style("stroke", "black");
}

function setup_data_occ() {
    data_occu = new Array();
    data_occu_null = new Array();
    data_occu_block = new Array();
    data_occu_image = new Array();

    for (ii = 0; ii < staircase[query_id].query_occu.length; ii++) {
        data_occu.push({
            x: (ii + 0.5) / staircase[query_id].query_occu.length,
            y: staircase[query_id].query_occu[ii] * 1
        });
        data_occu_null.push({
            x: (ii + 0.5) / staircase[query_id].query_occu.length,
            y: staircase[query_id].query_occu[ii] * 0
        });
        data_occu_block.push((ii + 0) / staircase[query_id].query_occu.length);
    }

    occu_image_appear = new Array();
    reset_occu_y(1);
}

function draw_text() {
	waterfall.append("svg:text")
        .attr("class", "copyright_text")
        .attr("x", function (d, i) {
        return $(window).width() - 67;
    })
        .attr("y", function (d, i) {
        return staircase_height;//$(window).height() - 44;
    })
        .attr("text-anchor", "end")
        .attr("fill", "black")
        .style("font", "9px sans-serif")
        .text("Hover on the heatmap to read news. The StatNews project, UC Berkeley. Copyrighted, The Regents of University of California 2012. All rights reserved.");
}

function draw_occu_line() {
	rules = waterfall.selectAll("g.rule")
        .data(y.ticks(3))
        .enter().append("svg:g")
        .attr("class", "occu_rule");

    rules.append("svg:line")
        .attr("class", "occu_horizontal_line")
        .attr("y1", 23)
        .attr("y2", 23)
        .attr("x1", margin_staircase * 5)
        .attr("x2", margin_staircase * 5 + w_staircase - margin_staircase * 5);
    rules.append("svg:text")
        .attr("class", "occu_tick")
        .attr("y", 23)
        .attr("x", margin_staircase * 5 - 3)
        .attr("dy", ".35em")
        .attr("text-anchor", "end")
        .text(function (d) {
            if (d3.max(staircase[query_id].query_occu) < 1) return Math.round(d * 1000) / 10 + "%";
            else return d;
    });

    d3.selectAll(".occu_tick").transition().duration(0).attr("y", y);
    d3.selectAll(".occu_horizontal_line").transition().duration(0).attr("y1", y).attr("y2", y);

    line_occu = d3.svg.line()
        .interpolate("cardinal")
        .x(function (d) {
        return x(d.x);
    })
        .y(function (d) {
        return y(d.y);
    });

    waterfall.append("svg:g").selectAll("path").data([1]).enter().append("svg:path")
        .attr("class", "occu_line_query")
        .attr("d", line_occu.tension(1)(data_occu_null))
        .attr("opacity", 0)
        .style("stroke", color_staircase_2[color_staircase_2.length - 1]);

    for (ii = 0; ii < data_occu_image.length; ii++)
        waterfall.append("svg:g").selectAll("path").data([1]).enter().append("svg:path")
            .attr("class", "occu_line_image")
            .attr("local_ii", ii)
            .attr("d", line_occu.tension(1)(data_occu_image[ii]))
            .attr("opacity", 0)
            .style("stroke-width", "1.7px")
            .style("stroke", function () {
            return "blue";
        });

    d3.selectAll(".occu_line_query").transition().duration(0).attr("d", line_occu.tension(1)(data_occu)).attr("opacity", 1);

    waterfall.append("svg:g").selectAll("path").data(data_occu).enter().append("svg:path")
        .attr("class", "occu_spike_index")
        .style("stroke-width", "0.1px")
        .style("fill", function (d, i) {
        return "white";
    })
        .style("stroke", "white")
        .attr("transform", function (d, i) {
        return "translate(" + x(d.x) + "," + (y(d.y) + 0) + ")";
    })
        .attr("d", d3.svg.symbol()
        .type(function (d, i) {
        return "circle";
    })
        .size(function (d, i) {
        return 0;
    }));

    waterfall.append("svg:g").selectAll("path").data(data_occu).enter().append("svg:path")
        .attr("class", "occu_spike_index")
        .style("stroke-width", "0.1px")
        .style("fill", function (d, i) {
        return "white";
    })
        .style("stroke", "white")
        .attr("transform", function (d, i) {
        return "translate(" + x(d.x) + "," + (y(d.y) + 0) + ")";
    })
        .attr("d", d3.svg.symbol()
        .type(function (d, i) {
        return "circle";
    })
        .size(function (d, i) {
        return 0;
    }));
}

function draw_waterfall() {
    waterfall.append("svg:g").selectAll("rect_occu")
        .data(data_occu_block)
        .enter().append("svg:rect")
        .attr("class", "occu_block")
        .attr("x", function (d) {
        return x(d);
    })
        .attr("width", function (d, i) {
        return (w_staircase - 2 - margin_staircase * 5) / col_num;
    })
        .attr("y", 33)
        .attr("height", margin_staircase * 1.5 + base_staircase - 2 - 33)
        .attr("fill", function (d, i) {
        return "#ffffff";
    })
        .attr("opacity", 1)
        .on("mouseover", function (d, i) {
        index_x = i;
        d3.selectAll(".stair_image").attr("opacity", function (d, i_) {
            if (d3.select(this).attr("local_i") == index_x)
                return 1;
            else
                return 0.2;
        });
        d3.selectAll(".stair_text_class").transition().delay(0).duration(0).attr("opacity", function (d, i_) {
            if (json_value[i_][index_x] > 0)
                return 1;
            else
                return 0;
        });
    })
        .on("mouseout", function () {

        d3.selectAll(".stair_image").attr("opacity", 1);
        d3.selectAll(".stair_text_class").transition().duration(0).attr("opacity", 1);
    })
        .append("title")
        .text(function (d, i) {

        temp = "";
        for (j = 0; j < json_text.length; j++)
            if (json_value[j][i] > 0)
                temp = temp + "<span class='art_i'>" + json_text[j] + "</span>, ";

        if (i > 0 && staircase[query_id].query_occu[i] > 0) {
            if (staircase[query_id].query_occu[i] - staircase[query_id].query_occu[i - 1] > 0)
                return "query: <span class=art_q>" + staircase[query_id].query + "</span><br>" + staircase[query_id].stair_tick_exact[i] + "&nbsp;-&nbsp;" + staircase[query_id].stair_tick_exact[i + 1] + "<br><br>news coverage: " + Math.round(staircase[query_id].query_occu[i] * 100000) / 1000 + "%" + " (<span style='color:#7DFF35'>" + Math.abs(Math.round(((staircase[query_id].query_occu[i] - staircase[query_id].query_occu[i - 1]) / staircase[query_id].query_occu[i - 1]) * 100000) / 1000) + "% UP</span>" + ")<br><br>" + "image words: " + temp.substr(0, temp.length - 2);
            else
                return "query: <span class=art_q>" + staircase[query_id].query + "</span><br>" + staircase[query_id].stair_tick_exact[i] + "&nbsp;-&nbsp;" + staircase[query_id].stair_tick_exact[i + 1] + "<br><br>news coverage: " + Math.round(staircase[query_id].query_occu[i] * 100000) / 1000 + "%" + " (<span style='color:#FF96FF'>" + Math.abs(Math.round(((staircase[query_id].query_occu[i] - staircase[query_id].query_occu[i - 1]) / staircase[query_id].query_occu[i - 1]) * 100000) / 1000) + "% DOWN</span>" + ")<br><br>" + "image words: " + temp.substr(0, temp.length - 2);
        } else
            return "query: <span class=art_q>" + staircase[query_id].query + "</span><br>" + staircase[query_id].stair_tick_exact[i] + "&nbsp;-&nbsp;" + staircase[query_id].stair_tick_exact[i + 1] + "<br><br>news coverage: " + Math.round(staircase[query_id].query_occu[i] * 100000) / 1000 + "%" + "<br><br>" + "image words: " + temp.substr(0, temp.length - 2);
    });
}

function draw_begin() {
	$("#image_sector_stair").html("");

	setup_waterfall();

    setup_data_occ();

    draw_occu_line();

    draw_text();

    draw_stair_tick_lines();

    draw_begin_text();
}

function draw() {
    staircase_height = 170 + stair_image_height * json_text.length;
    waterfall.selectAll("rect_stair").remove();
    d3.selectAll(".stair_text_class").remove();
    d3.selectAll(".stair_image").remove();
    //d3.selectAll(".stair_horizontal_line").remove();
    d3.selectAll(".stair_tick_class").remove();
    d3.selectAll(".stair_tick").remove();
    d3.selectAll(".occu_rule").remove();
    d3.selectAll(".small_horizontal_line").remove();
    d3.selectAll(".copyright_text").remove();

    waterfall_intensity_1 = d3.scale.quantize()
        .domain([d3.min(d3.merge(json_value)) * 1 - 0, d3.max(d3.merge(json_value)) / 2.3])
        .range(color_staircase_1);
    waterfall_intensity_2 = d3.scale.quantize()
        .domain([d3.min(d3.merge(json_value)) * 1 - 0, d3.max(d3.merge(json_value)) / 2.3])
        .range(color_staircase_2);

    draw_stair();    

    draw_text();

    draw_stair_tick_lines();

    draw_horizontal_line();
    //draw_begin_text();
}

function draw_stair() {
    for (ii = 0; ii < json_value.length; ii++) {
        waterfall/*.append("svg:g")*/.selectAll("rect_stair")
            .data(json_value[ii])
            .enter().append("svg:rect")
            .attr("class", "stair_image")
            .attr("x", function (d, i) {
	            return i * (w_staircase - 2 - margin_staircase * 5) / col_num + margin_staircase * 5 + 1;
	        })
            .attr("y", function (d, i) {
	            /*return ii * (h_staircase - margin_staircase * 1.5) / row_num + margin_staircase * 1.5 + base_staircase;*/
                return ii * stair_image_height + margin_staircase * 1.5 + base_staircase;
	        })
            .attr("width", function (d, i) {
	            if (d == 0)
	                return 0;
	            else
	                return (w_staircase - 2 - margin_staircase * 5) / col_num;
	        })
            .attr("height", function (d, i) {
	            if (d == 0)
	                return 0;
	            else {
                    /*staircase_height += stair_image_height;*/
                    return stair_image_height;
	                //return (h_staircase - margin_staircase * 1.5) / row_num;
                }
	        })
            .attr("opacity", 0)
            .attr("fill", function (d, i) {
	            if (d == 0) return "white";
	            else if (staircase[query_id].stair_color !== undefined && staircase[query_id].stair_color[i] == 1) return waterfall_intensity_1(d);
	            else return waterfall_intensity_2(d);
	        })
	            .attr("local_ii", ii)
	            .attr("local_i", function (d, i) {
	            return i;
        	});
    }

    d3.selectAll(".stair_image").transition().delay(function () {
        return (d3.select(this).attr("local_i") * 0.2 + d3.select(this).attr("local_ii") * 0.33) * 36;
    }).duration(function () {
        return 0;
    }).attr("opacity", 1);

    vertical_text = waterfall/*.append("svg:g")*/.selectAll("json_text")
        .data(json_text)
        .enter().append("svg:text")
        .attr("class", "stair_text_class")
        .attr("x", function (d) {
        return margin_staircase * 4.5;
    })

    .attr("y", function (d, i) {
        return i * stair_image_height + margin_staircase * 1.5 + base_staircase + stair_image_height * 0.8;
    })

    .attr("text-anchor", "end")
        .attr("fill", function (d, i) {
	        if (d.substring(0, 4) == "PSTV")
	            return "green";
	        if (d.substring(0, 4) == "NGTV")
	            return "red";
	        return "black";
	    })
        .attr("local_ii", function (d, i) {
	        return i;
	    })
        .attr("opacity", 0)
        .style("font", function (d, i) {
	        return waterfall_font_size + "px sans-serif";
	    })
        .text(function (d, i) {
	        d = d.replace(/CON/g, ' ');

	        if (d.substring(0, 4) == "PSTV" || d.substring(0, 4) == "NGTV")
	            return d.substring(4);
	        else
	            return d;
	    });


    d3.selectAll(".stair_text_class").transition().delay(function (d, i) {
        return i * 1;
    }).duration().attr("opacity", 1);
}

function draw_horizontal_line() {
    var staircase_horizontal_tick = new Array();

    console.log(json_text);

    for (var i = 0; i < json_text.length; i++) {
        var count_col = 0;
        var first = false;

        for (var j = 0; j < 23; j++) {
            if (json_value[i][j] == 0 && !first) {
                count_col += 1;
            }

            if (json_value[i][j] > 0 && !first) {
                first = true;

                staircase_horizontal_tick[i] = count_col;
            }
        }
    }

    console.log(staircase_horizontal_tick);

    /*(h_staircase - margin_staircase * 1.5) / row_num*/

    waterfall/*.append("svg:g")*/.selectAll("staircase_horizontal_tick")
        .data(staircase_horizontal_tick).enter().append("svg:line")
        .attr("class", "small_horizontal_line")
        .attr("x1", function (d, i) {
        return margin_staircase * 4.5;
    })
        .attr("y1", function (d, i) {
        return i * stair_image_height + margin_staircase * 1.5 + base_staircase + stair_image_height * 0.8;
    })
        .attr("x2", function (d) {
        return margin_staircase * 5 + d * (w_staircase - margin_staircase * 5) / col_num - 1;
    })
        .attr("y2", function (d, i) {
        return i * stair_image_height + margin_staircase * 1.5 + base_staircase + stair_image_height * 0.8;
    })
        .attr("stroke", "#ddd")
        .attr("stroke-width", 0.5);
}

function draw_stair_tick_lines() {
	staircase_tick_period = 4;
    staircase_tick_offset = 0;

    waterfall/*.append("svg:g")*/.selectAll("staircase[query_id].stair_tick")
        .data(staircase[query_id].stair_tick)
        .enter().append("svg:line")
        .attr("class", "stair_tick")
        .attr("y1", margin_staircase * 0.9)
        .attr("y2", margin_staircase * 0.9 + /*base_staircase +*/ staircase_height - margin_staircase * 1.5)
        .attr("x1", function (d, i) {
        	return i * (w_staircase - margin_staircase * 5) / col_num + margin_staircase * 5;
    })
        .attr("x2", function (d, i) {
        	return i * (w_staircase - margin_staircase * 5) / col_num + margin_staircase * 5;
    })
        .attr("stroke-width", "0.3px")
        .attr("visibility", function (d, i) {
	        if (!((i + staircase_tick_offset) % staircase_tick_period)) return "visible";
	        else return "hidden";
    })
        .attr("stroke", "#000");

    waterfall/*.append("svg:g")*/.selectAll("stair_tick")
        .data(staircase[query_id].stair_tick)
        .enter().append("svg:text")
        .attr("class", "stair_tick_class")
        .attr("y", function (d, i) {
	        if (i % 2 == 0) return margin_staircase * 0.8;
	        else return margin_staircase * 0.8;
    })
        .attr("x", function (d, i) {
        	return i * (w_staircase - margin_staircase * 5) / col_num + margin_staircase * 5 - 2;
    })
        .attr("visibility", function (d, i) {
	        if (!((i + staircase_tick_offset) % staircase_tick_period) ) return "visible";
	        else return "hidden";
    })
        .attr("font-size", "10px")
        .attr("text-anchor", "middle")
        .text(function (d) {
        	return d;
    });

    waterfall.append("svg:line")
        .attr("class", "stair_tick_class")
        .attr("x1", margin_staircase * 5)
        .attr("y1", margin_staircase * 0.9)
        .attr("x2", margin_staircase * 5)
        .attr("y2", margin_staircase * 0.9 + /*base_staircase +*/ staircase_height - margin_staircase * 1.5)
        .attr("fill", "white")
        .style("fill-opacity", 0)
        .style("stroke-width", "0.7px")
        .style("stroke", "black");
}

function draw_begin_text() {
	waterfall.append("svg:text")
        .attr("x", function (d, i) {
        return margin_staircase * 0.5;
    })
        .attr("y", function (d, i) {
        return margin_staircase * 0.3;
    })
        .attr("text-anchor", "begiin")
        .attr("fill", "#000000")
        .style("font", "14px sans-serif")
        .text(staircase[query_id].title);
}